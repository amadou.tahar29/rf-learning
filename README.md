## Devoir de Reinforcement Learning 
### 27/04/2023
### Pierre Ovaere - Tanguy Varet - Donwoung Dany Sandra - Amadou Hamadou Tahar - Tanguy Moynot

Ces fichiers permettent l'implémentation d'un jeu de Dames. Nous avons connu des difficultés et ne sommes pas parvenus à implémenter la partie RL pour pouvoir jouer contre une IA aux dames: Voici les différents fichiers et leur rôles dans nos différents essais d'implémenter un jeu de dames par RL.

## Installation
Il est nécessaire pour lancer ces fichiers sur python d'avoir installé pygame avec la commande: pip install "pygame" ou python3 install pygame .

Tentative 1
Fichiers: 
1) board.py
Ce fichier contient les classes "Board" et "Piont" qui renvoient au tableau de damier et aux pions respectivement.
2) main.py
Ce fichier lance le jeu et fait apparaitre en sortie de notebook les coups joués par les différents joueurs.

Classes:
1) "Board":

__init__(self, width:int, height:int)
permet de construire le damier avec la longueur et la largeur spécifiée.

draw(self, screen:Surface) -> None
permet de faire apparaitre le damier construit ainsi que les pions.

move(self, dx:int, dy:int, piont:Piont) -> bool
fait bouger l'instance spécifiée de la classe "Piont" sur la nouvelle position. Si le pion peut encore bouger, La fonction retourne TRUE et FALSE sinon.

2) "Piont":

__init__(self, x:int, y:int, color:tuple, caseSize:int) -> None
Initialise les pions du damier avec pour coordonnées x et y et pour couleur "WHITE" ou "Black".

move(self, dx:int, dy:int) -> None
bouge un pion sur ses nouvelles coordonnées x et y.

draw(self, screen:Surface) -> None
Fait apparaitre le pion sur sa nouvelle case sur le damier.

Tentative 2
Fichier:
AI_functions
Ce fichier contient les fonctions implémentant les concepts d'êtat et de récompenses.

Fonctions: 
1)flip_coin(prob)
Variable aléatoire binaire de parametre prob
2)checkers_features(state, action)
Retourne la liste des acions disponibles au joueur du tour
3)checkers_reward(state, action, next_state)
retourne une valeur numérique en "récompense" à l'action choisie

Classes:
1)"Agent" (classe abstraite):

__init__(self, is_learning_agent=False)
crée une IA "agent" qui peut être entrainée

get_action(self, state)
retourne la meilleure action à jouer en fonction de l'etat actuel

2)"KeyBoardAgent" (sa fonction get_action diffère)

3)"ReinforcementLearningAgent"

update(self, state, action, next_state, reward)
met à jour l'agent sur la nouvelle situation suite à son action)

start_episode(self):
retrace l'ensemble des récompenses et etats traversés durant l'entrainement de l'agent

4)"QLearningAgent" (hérite de ReinforcementLearningAgent)

get_q_value(self, state, action, features)

compute_value_from_q_values(self, state)

compute_action_from_q_values(self, state,actions)

getPolicy(self, state)

getValue(self, state)











