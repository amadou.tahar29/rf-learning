#  Author  : Pierre
from pygame import *

BLACK = (0,0,0)
WHITE = (255,255,255)
#  Piont class
class Piont:
    """
    Piont class
    """
    def __init__(self, x:int, y:int,color:tuple,caseSize:int)->None:
        """
        Init the piont

        Args:
            x (int): x position
            y (int): y position
            color (tuple): color of the piont
            caseSize (int): size of the piont
        """
        self.x = x
        self.y = y
        self.color = color
        self.size = caseSize
        self.isOut = False
        self.isDame = False
        self.selected = False

    def move(self, dx:int, dy:int)->None:
        """
        Move the piont to the new position

        Args:
            dx (int): new x position
            dy (int): new y position
        """
        self.x = dx
        self.y = dy

    def draw(self, screen:Surface)->None:
        """
        Draw the piont on the screen

        Args:
            screen (Surface): screen to draw on
        """
        if not self.isOut:
            surface = Surface((self.size,self.size))
            if not self.selected :
                if self.color == WHITE:
                    draw.circle(surface, BLACK, (self.size//2, self.size//2), self.size//2)
                else:
                    draw.circle(surface, WHITE, (self.size//2, self.size//2), self.size//2)
            else:
                draw.circle(surface,((255,0,0)), (self.size//2, self.size//2), self.size//2)
            draw.circle(surface, self.color, (self.size//2, self.size//2), self.size//2.2)
            if self.isDame:
                draw.circle(surface,((0,255,0)),(self.size//2, self.size//2),self.size//4)
            screen.blit(surface, (self.x, self.y))


# Board class
class Board:
    """
    Board class to manage the game
    """
    def __init__(self, width:int, height:int)->None:
        """
        Constructor of the board

        Args:
            width (int): Width of the board
            height (int): Height of the board
        """
        self.width = width
        self.height = height
        self.pionts_WHITE = []
        self.pionts_BLACK = []
        self.caseSize = width // 10
        self.map = list(list(None for _ in range (10))for _ in range(10))
        self.__init_board()

    def draw(self, screen:Surface)->None:
        """
        Draw the board

        Args:
            screen (Surface): Surface to draw on
        """
        ## Draw case    
        self.__draw_board(screen)
        ## Draw pionts
        for piont in self.pionts_BLACK:
            piont.draw(screen)
        for piont in self.pionts_WHITE:
            piont.draw(screen)


    def move(self, dx:int, dy:int,piont:Piont)->bool:
        """
        Move the piont to the new position
        Alse check if the piont eat an enemy

        Args:
            dx (int): new x position
            dy (int): new y position
            piont (Piont): piont to move
        
        Returns:
            bool: True if the piont can move again, False else
        """
        # Check if the player can move
        print(f"Move pts to : {dy}-{dx}")
        if self.can_move(piont,dx,dy):
            # Check if the player can eat
            x = dx//self.caseSize
            y = dy//self.caseSize
            px = piont.x//self.caseSize
            py = piont.y//self.caseSize
            # Calculate the delta
            move = abs(py-y)
            asEat = False
            # If the move is more than 2 case
            if not piont.isDame and move >2:
                return True
            # Check for eatable enemi
            for i in range (move):
                # If the move is more than 1 cell after the enemi
                if asEat and i < move:
                    self.map[cy][cx] = case
                    case.isOut = False
                    if case.color == BLACK:
                        self.pionts_BLACK.append(case)
                    if case.color == WHITE:
                        self.pionts_WHITE.append(case)
                    return True
                # Get the next cell
                case = None
                if py > y:
                    case =self.map[py-i]
                else:
                    case = self.map[py+i]
                if px > x:
                    case = case[px-i]
                else:
                    case = case[px+i]
                # Check if the cell is not empty or not the player
                if case != None and case != piont :
                    # If the case is an enemi
                    if case.color != piont.color:
                        case.isOut = True
                        if case.color == BLACK :
                            self.pionts_BLACK.remove(case)
                        if case.color == WHITE :
                            self.pionts_WHITE.remove(case)
                        print(f"Current Game config : \n\t->NB of white P : {len(self.pionts_WHITE)} \n\t->Nb of black P : {len(self.pionts_BLACK)} ")
                        cx = case.x//self.caseSize
                        cy = case.y//self.caseSize
                        self.map[cy][cx] = None
                        asEat = True
                    else:
                        # Else we can't move
                        return False
            # If we try to move more than 1 cell without eating, not possible
            if move >= 2 and not asEat and not piont.isDame:
                return True
            # Move the player
            piont.move(dx, dy)
            self.map[y][x] = piont
            self.map[py][px] = None
            # Check if can play an other turn
            if y == 0 and piont.color == WHITE :
                piont.isDame = True
            if y == 9 and piont.color == BLACK :
                piont.isDame = True
            return asEat and self.check_can_move_next(y,x,piont)
        return True
            
        
    def check_can_move_next(self,y:int,x:int,piont:Piont)->bool:
        """
        Check if the player can play again

        Args:
            y (int): y position of the piont
            x (int): x position of the piont
            piont (Piont): piont to check
        
        Returns:
            bool: True if the player can play again, False else
        """
        # Check if there the player can play again
        if piont.color == WHITE:
            mv = -1
        else:
            mv = 1
        movement = []
        if not piont.isDame:
            if y+(mv*2) > 9 or y+(mv*2) < 0 : 
                return False
            if x-(mv*2) <10 and x-(mv*2) >=0  :
                movement.append(self.map[y+(mv*2)][x-(mv*2)]  == None and self.map[y+mv][x-mv] != None)
            if x+(mv*2) <10 and x+(mv*2) >= 0 :
                movement.append(self.map[y+(mv*2)][x+(mv*2)]  == None and self.map[y+mv][x+mv] != None)
        else:
            for i in range(2,10):
                # Check forward
                if y+(mv*i) > 9 or y+(mv*i) < 0: 
                    continue
                if x-(mv*i) <10 and x-(mv*i) >=0  :
                    movement.append(self.map[y+(mv*i)][x-(mv*i)]  == None and self.map[y+(mv*(i-1))][x-(mv*(i-1))] != None and self.map[y+(mv*(i-1))][x-(mv*(i-1))].color != piont.color)
                if x+(mv*i) <10 and x+(mv*i) >= 0 :
                    movement.append(self.map[y+(mv*i)][x+(mv*i)]  == None and self.map[y+(mv*(i-1))][x+(mv*(i-1))] != None and self.map[y+(mv*(i-1))][x+(mv*(i-1))].color != piont.color)
                # Check backward
                if y-(mv*i) > 9 or y-(mv*i) < 0: 
                    continue
                if x-(mv*i) <10 and x-(mv*i) >=0  :
                    movement.append(self.map[y-(mv*i)][x-(mv*i)]  == None and self.map[y-(mv*(i-1))][x-(mv*(i-1))] != None and self.map[y-(mv*(i-1))][x-(mv*(i-1))].color != piont.color)
                if x+(mv*i) <10 and x+(mv*i) >= 0 :
                    movement.append(self.map[y-(mv*i)][x+(mv*i)]  == None and self.map[y-(mv*(i-1))][x+(mv*(i-1))] != None and self.map[y-(mv*(i-1))][x+(mv*(i-1))].color != piont.color)
        return any(movement)
            
    def can_move(self,piont:Piont,dx:int,dy:int):
        """
        Check if the player can move to the destination

        Args:
            piont (Piont): piont to move
            dx (int): destination x
            dy (int): destination y
        """
        #  Check all basic mouvement
        # Check if point don't go out of the board
        if dx < 0 or dx > self.width:
            return False
        if dy< 0 or dy > self.height:
            return False
        
        # Check if the destination is correct
        if piont.x == dx or piont.y == dy:
            return False
        
        # Check if the destination is free
        x = dx//self.caseSize
        y = dy//self.caseSize
        if self.map[y][x] != None:
            return False
        
        # check if it's diagonal
        if abs(piont.x - dx) != abs(piont.y - dy):
            return False
        
        if not piont.isDame:
            # Check if the destination is in the right direction
            if piont.color == WHITE and piont.y <= dy:
                print("Can't go back !")
                return False
            if piont.color == BLACK and piont.y >= dy:
                print("Can't go back !")
                return False
            
        return True
    


    def __draw_board(self,screen:Surface)->None:
        """
        Draw the board on the screen

        Args:
            screen (Surface): screen to draw on
        """
        # Draw board on screen
        isWhite = False
        for i in range(0,self.width,self.caseSize):
            for j in range(0,self.height,self.caseSize):
                # Define rect
                case = Surface((self.caseSize,self.caseSize))
                if isWhite:
                    isWhite = False
                    case.fill(WHITE)
                else:
                    isWhite = True
                    case.fill(BLACK)
                screen.blit(case,(i,j))
            isWhite = not isWhite
        draw.rect(screen, (255, 255, 255), (0, 0, self.width, self.height), 1)


    def __init_board(self)->None:
        """
        Init the board 
        """
        for y in range(len(self.map)):
            for x in range(len(self.map[y])):
                if y < 4 :
                    if (y %2 ==0 and x %2 == 0) or (y %2 ==1 and x %2 == 1):
                        self.map[y][x] = Piont(x*self.caseSize,y*self.caseSize,BLACK,self.caseSize)
                        self.pionts_BLACK.append(self.map[y][x])

                elif y >= 6 :
                    if (y %2 ==0 and x %2 == 0) or (y %2 ==1 and x %2 == 1):
                        self.map[y][x] = Piont(x*self.caseSize,y*self.caseSize,WHITE,self.caseSize)
                        self.pionts_WHITE.append(self.map[y][x])

