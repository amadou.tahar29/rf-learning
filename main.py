# Auther : Pierre²
from pygame import *
from board import *



class IA_Learning(Board):

    # TODO: Implement
    def __init__(self,width,height):
        super().__init__(width, height)
        self.reward = None
        #TODO : Add Ia variable to learn


# Main
# ---------------------- Pygame setup ---------------------------------------
screen = display.set_mode((600, 600))
display.set_caption("DAMES ")
# ----------------------------------------------------------------------------

# --------------------------- Board init -------------------------------------
board = IA_Learning(600, 600)

# --------------------------------- Game Var ---------------------------------
running = True
pts = None
player = WHITE
replay = False
# -------------------------------- Main While --------------------------------
while running:
    for e in event.get():
        if e.type == QUIT:
            # Stop Game
            running = False
        if e.type == MOUSEBUTTONDOWN:
            # If Mouse  button presse down on board
            # Calculate Position of the cell in the board
            pos = e.pos
            x = pos[0]//board.caseSize
            y = pos[1]//board.caseSize
            dx = x*board.caseSize
            dy = y*board.caseSize
            # If there is no pion selected :
            if (pts == None or pts.color == player) and not replay :
                if pts != None :
                    pts.selected = False
                ptsN = board.map[y][x]
                # If it's a valid pion selected
                if ptsN != None and ptsN.color == player:
                    # We mark the pion as selected
                    pts = ptsN
                    pts.selected = True
                    replay = False
                    continue
            if pts != None :
                # If a pion is selected
                if not board.move(dx,dy,pts):
                    # If the board.move() return false, no other move is possible
                    pts.selected = False
                    pts = None
                    replay = False
                    # We switch player turn
                    if player == BLACK:
                        player = WHITE
                    else:
                        player = BLACK
                else:
                    replay = True
    # Display the board
    screen.fill((0, 0, 0))
    board.draw(screen)
    if len(board.pionts_WHITE) == 0 or len(board.pionts_BLACK) == 0:
        running = False
    display.flip()
quit()