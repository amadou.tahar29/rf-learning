from abc import ABC, abstractmethod
import random
import numpy as np



CHECKERS_FEATURE_COUNT = 8
WIN_REWARD = 500
LOSE_REWARD = -500
LIVING_REWARD = -0.1




def flip_coin(prob):
	return random.random() < prob

def checkers_features(state, action):
    """
    state: game state of the checkers game
    action: action for which the feature is requested

    Returns: list of feature values for the agent whose turn is in the current state
    """
    next_state = state.generate_successor(action, False)

    agent_ind = 0 if state.is_first_agent_turn() else 1
    oppn_ind = 1 if state.is_first_agent_turn() else 0

    num_pieces_list = state.get_pieces_and_kings()

    agent_pawns = num_pieces_list[agent_ind]
    agent_kings = num_pieces_list[agent_ind + 2]
    agent_pieces = agent_pawns + agent_kings

    oppn_pawns = num_pieces_list[oppn_ind]
    oppn_kings = num_pieces_list[oppn_ind + 2]
    oppn_pieces = oppn_pawns + oppn_kings


    num_pieces_list_n = next_state.get_pieces_and_kings()

    agent_pawns_n = num_pieces_list_n[agent_ind]
    agent_kings_n = num_pieces_list_n[agent_ind + 2]
    agent_pieces_n = agent_pawns_n + agent_kings_n

    oppn_pawns_n = num_pieces_list_n[oppn_ind]
    oppn_kings_n = num_pieces_list_n[oppn_ind + 2]
    oppn_pieces_n = oppn_pawns_n + oppn_kings_n

    features = []

    # features.append(agent_pawns_n - agent_pawns)
    # features.append(agent_kings_n - agent_kings)
    # features.append(agent_pieces_n - agent_pieces)

    # pawns and kings of agent and opponent in current state
    features.append(agent_pawns)
    features.append(agent_kings)
    features.append(oppn_pawns)
    features.append(oppn_kings)

    features.append(oppn_pawns_n - oppn_pawns)
    features.append(oppn_kings_n - oppn_kings)
    features.append(oppn_pieces_n - oppn_pieces)

    features.append(next_state.num_attacks())

    # print(features)
    return features


def checkers_reward(state, action, next_state):

    if next_state.is_game_over():
        # infer turn from current state, because at the end same state is used by both agents
        if state.is_first_agent_turn():
            return WIN_REWARD if next_state.is_first_agent_win() else LOSE_REWARD
        else:
            return WIN_REWARD if next_state.is_second_agent_win() else LOSE_REWARD

    agent_ind = 0 if state.is_first_agent_turn() else 1
    oppn_ind = 1 if state.is_first_agent_turn() else 0

    num_pieces_list = state.get_pieces_and_kings()

    agent_pawns = num_pieces_list[agent_ind]
    agent_kings = num_pieces_list[agent_ind + 2]

    oppn_pawns = num_pieces_list[oppn_ind]
    oppn_kings = num_pieces_list[oppn_ind + 2]

    num_pieces_list_n = next_state.get_pieces_and_kings()

    agent_pawns_n = num_pieces_list_n[agent_ind]
    agent_kings_n = num_pieces_list_n[agent_ind + 2]

    oppn_pawns_n = num_pieces_list_n[oppn_ind]
    oppn_kings_n = num_pieces_list_n[oppn_ind + 2]

    r_1 = agent_pawns - agent_pawns_n
    r_2 = agent_kings - agent_kings_n
    r_3 = oppn_pawns - oppn_pawns_n
    r_4 = oppn_kings - oppn_kings_n

    reward = r_3 * 0.2 + r_4 * 0.3 + r_1 * (-0.4) + r_2 * (-0.5)

    if reward == 0:
        reward = LIVING_REWARD

    return reward






class Agent(ABC):

    def __init__(self, is_learning_agent=False):
        self.is_learning_agent = is_learning_agent
        self.has_been_learning_agent = is_learning_agent

    @abstractmethod
    def get_action(self, state):
        """
        state: the state in which to take action
        Returns: the single action to take in this state
        """
        pass


class KeyBoardAgent(Agent):

    def __init__(self):
        Agent.__init__(self)


    def get_action(self, state):
        """
        state: the current state from which to take action

        Returns: list of starting position, ending position
        """

        start = [int(pos) for pos in input("Enter start position (e.g. x y): ").split(" ")]
        end = [int(pos) for pos in input("Enter end position (e.g. x y): ").split(" ")]

        ends = []
        i=1
        while i < len(end):
            ends.append([end[i-1], end[i]])
            i += 2

        action = [start] + ends
        return action
class ReinforcementLearningAgent(Agent):

    def __init__(self, is_learning_agent=True):
        Agent.__init__(self, is_learning_agent)

        self.episodes_so_far = 0


    @abstractmethod
    def get_action(self, state):
        """
        state: the current state from which to take action

        Returns: the action to perform
        """
        # TODO call do_action from this method
        pass


    @abstractmethod
    def update(self, state, action, next_state, reward):
        """
        performs update for the learning agent

        state: the state (s) in which action was taken
        action: the action (a) taken in the state (s)
        next_state: the next state (s'), in which agnet will perform next action, 
                    that resulted from state (s) and action (a)
        reward: reward obtained for taking action (a) in state (s) and going to next state (s')
        """
        pass

    def start_episode(self):
        # Accumulate rewards while training for each episode and show total rewards 
        # at the end of each episode i.e. when stop episode
        self.prev_state = None
        self.prev_action = None

        self.episode_rewards = 0.0


    def stop_episode(self):
        # print('reward this episode', self.episode_rewards)
        pass

    @abstractmethod
    def start_learning(self):
        pass


    @abstractmethod
    def stop_learning(self):
        pass


    @abstractmethod
    def observe_transition(self, state, action, next_state, reward, next_action=None):
        pass


    @abstractmethod
    def observation_function(self, state):
        pass


    # TODO
    def reward_function(self, state, action, next_state):
        # make a reward function for the environment
        return checkers_reward(state, action, next_state)


    def do_action(self, state, action):
        """
        called by get_action to update previous state and action
        """
        self.prev_state = state
        self.prev_action = action


class QLearningAgent(ReinforcementLearningAgent):

    def __init__(self, alpha=0.01, gamma=0.1, epsilon=0.5, is_learning_agent=True, weights=None):

        """
        alpha: learning rate
        gamma: discount factor
        epsilon: exploration constant
        is_learning_agent: whether to treat this agent as learning agent or not
        weights: default weights
        """

        ReinforcementLearningAgent.__init__(self, is_learning_agent=is_learning_agent)

        self.original_alpha = alpha
        self.original_epsilon = epsilon

        self.alpha = alpha
        self.gamma = gamma
        self.epsilon = epsilon

        if not is_learning_agent:
            self.epsilon = 0.0
            self.alpha = 0.0


        if weights is None:
            # initialize weights for the features
            self.weights = np.zeros(CHECKERS_FEATURE_COUNT)
        else:
            if len(weights) != CHECKERS_FEATURE_COUNT:
                raise Exception("Invalid weights " + weights)

            self.weights = np.array(weights, dtype=float)


    def start_learning(self):
        """
        called by environment to notify agent of starting new episode
        """

        self.alpha = self.original_alpha
        self.epsilon = self.original_epsilon

        self.is_learning_agent = True


    def stop_learning(self):
        """
        called by environment to notify agent about end of episode
        """
        self.alpha = 0.0
        self.epsilon = 0.0

        self.is_learning_agent = False


    def get_q_value(self, state, action, features):
        """
          Returns: Q(state,action)
        """
        q_value = np.dot(self.weights, features)
        return q_value


    def compute_value_from_q_values(self, state):
        """
          Returns: max_action Q(state, action) where the max is over legal actions.
                   If there are no legal actions, which is the case at the terminal state, 
                   return a value of 0.0.
        """
        actions = state.get_legal_actions()

        if not actions:
            return 0.0

        q_values = \
        [self.get_q_value(state, action, checkers_features(state, action)) for action in actions]

        return max(q_values)


    def compute_action_from_q_values(self, state, actions):
        """
          Returns: the best action to take in a state. If there are no legal actions,
                   which is the case at the terminal state, return None.
        """
        if not actions:
            return None

        # if max_value < 0:
        #     return random.choice(actions)

        arg_max = np.argmax([self.get_q_value(state, action, checkers_features(state, action)) 
            for action in actions])

        return actions[arg_max]


    def get_action(self, state):
        """
          Returns: the action to take in the current state.  With probability self.epsilon,
                   take a random action and take the best policy action otherwise.  If there are
                   no legal actions, which is the case at the terminal state, returns None.
        """

        # Pick Action
        legal_actions = state.get_legal_actions()
        action = None

        if not legal_actions:
            return None

        if flip_coin(self.epsilon):
            action = random.choice(legal_actions)
        else:
            action = self.compute_action_from_q_values(state, legal_actions)

        self.do_action(state, action)
        return action


    def update(self, state, action, next_state, reward):

        features = checkers_features(state, action)

        expected = reward + self.gamma * self.compute_value_from_q_values(next_state)
        current = self.get_q_value(state, action, features)

        temporal_difference = expected - current

        for i in range(CHECKERS_FEATURE_COUNT):
            self.weights[i] = self.weights[i] + self.alpha * (temporal_difference) * features[i]


    def getPolicy(self, state):
        return self.compute_action_from_q_values(state, state.get_legal_actions())


    def getValue(self, state):
        return self.compute_value_from_q_values(state)  


    def observe_transition(self, state, action, next_state, reward, next_action=None):
        """
        state: the state (s) in which action was taken
        action: the action (a) taken in the state (s)
        next_state: the next state (s'), in which agnet will perform next action, 
                    that resulted from state (s) and action (a)
        reward: reward obtained for taking action (a) in state (s) and going to next state (s')
        """
        self.episode_rewards += reward
        self.update(state, action, next_state, reward)


    def observation_function(self, state):
        if self.prev_state is not None:
            reward = self.reward_function(self.prev_state, self.prev_action, state)
            # print('reward is', reward)
            self.observe_transition(self.prev_state, self.prev_action, state, reward)

    def update_parameters(self, freq, num_games):
        if num_games % freq == 0:
            self.original_alpha /= 2.0
            self.original_epsilon /= 2.0